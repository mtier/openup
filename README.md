# OpenUp

openup is a small utility for OpenBSD that can be run standalone or from cron(8) and that checks for security updates in both packages and the base system. openup uses the regular pkg tools, it does not implement anything on top.

It's primary usecase is for use with the [M:Tier Stable Packages](https://stable.mtier.org/).

## Usage

	Usage: openup [-K][-S|c]
	
	Options:
	  -K    do not check for kernel binpatches (when running non GENERIC)
	  -S    ignore binpatch/package signatures
	  -c    check/cron mode, report only (cannot be used with -S)

It can operate in 2 different modes.

## Standalone mode

When running interactively, openup is typically used to update and/or install stable packages and binpatches. It is a standalone utility which means all settings are configured within the application itself (it does not follow `PKG_PATH` nor installpatch from `pkg.conf(5)`).

Here are the steps performed when openup is run without any option:
- check if there is a new openup version and if so warn the user and quit
- check if the certificate used to install signed packages it properly installed and if not, download and install it
- update and install missing binpatches (base system)
- update stable packages

	# openup
	===> Checking for openup update
	===> Installing/updating binpatch(es)
	binpatch53-i386-kernel-3.0->4.0: ok
	binpatch53-i386-tftpd-1.0: ok
	Read shared items: ok
	Packages with signatures: 2
	===> Updating package(s)
	curl-7.26.0p2->7.26.0p3: ok
	Read shared items: ok
	Packages with signatures: 1
	!!! 
	!!! System must be rebooted after the last kernel update
	!!!

As seen in this example, openup always displays the commands it is about to run.

### Cron mode

Running openup with the '-c' option will only display vulnerable packages without making any modification to the system. The prefered way of running like this is from cron(8) once or twice a day; if openup outputs anything, it will be mailed out to root.

	# openup -c
	--- binpatch53-i386-kernel ---
	Available update(s): 4.0 OpenBSD errata 007: Two flaws in the vio(4)
	driver may cause a kernel panic, and may cause IPv6 neighbour discovery
	to fail due to multicast receive problems.
	--- binpatch53-i386-tftpd ---
	Available update(s): 1.0 OpenBSD errata 006: A denial of services was
	discovered where certain combinations of TFTP options could cause OACK
	generation to fail, which in turn caused a double free in tftpd.
	--- curl ---
	Available update(s): 7.26.0p3 cURL: A vulnerability has been reported in
	libcURL, which can be exploited by malicious people to potentially
	compromise an application using the library.

### Options

openup will use some default values which may not fit very well within a particular site. By creating `/etc/openup.conf` listing override variables, it is possible to configure several things:

- OPENUP_URL
  - default: `https://stable.mtier.org/openup`
  - description: URL to the latest open version
- PKG_CERT_URL
  - default: `https://stable.mtier.org/mtier.cert`
  - description: URL to the certificate used to sign the stable packages
- PKG_CERT_FPRINT
  - default: DE:29:0F:7F:B8:0E:36:5A:AF:A9:BF:E0:4E:08:C2:0F:2D:50:16:97
  - description: fingerprint of the certificate listed in PKG_CERT_URL
- PKG_PATH_MAIN
  - default: `http://ftp.fr.openbsd.org/pub/OpenBSD/$(uname -r)/packages/$(arch -s)`
  - description: `PKG_PATH` of the currently running OpenBSD release
- PKG_PATH_UPDATE
  - default: `https://stable.mtier.org/updates/$(uname -r)/$(arch -s)`
  - description: `PKG_PATH` for the corresponding release stable packages service
- VUXML_URL
  - default: `https://stable.mtier.org/vuxml/$(uname -r | tr -d '.').xml`
  - description: URL for the current OpenBSD release of the latest vulnerabilities database

## License

openup is distributed under the terms of the ISC license.
